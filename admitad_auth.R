admitad_auth <- function(proxy_usage = F, refresh_token = F){
  library(httr)
  library(jsonlite)
  
  body <- paste0("client_id=", Sys.getenv("ADMITAD_CLIENT_ID"),
                 "&client_secret=", Sys.getenv("ADMITAD_CLIENT_SECRET"))
  
  body <- if(identical(refresh_token, F)){
    paste0(body, "&grant_type=client_credentials&scope=advertiser_statistics")
  } else {
    paste0(body, "&grant_type=refresh_token&refresh_token=", refresh_token)
  }
  

  url <- "https://api.admitad.com/token/"
  auth <- paste("Basic", Sys.getenv("ADMITAD_AUTH"))
  ct <- "application/x-www-form-urlencoded"
  
  #setting up proksi usage
  reset_config()
  if (!identical(proxy_usage, F)) {
    proksi <- use_proxy(url = proxy_usage$url,
                        port = proxy_usage$port,
                        username = proxy_usage$username,
                        password = proxy_usage$password,
                        auth = proxy_usage$auth)
    set_config(proksi)
    set_config(httr::config(ssl_verifypeer = proxy_usage$ssl))
  }
  
  x <- POST(url, body = body, add_headers(Authorization = auth), content_type(ct))
  x <- fromJSON(content(x, "text", encoding = "UTF-8"))
  x$expiration_date <- Sys.Date() + x$expires_in / 60 / 60 / 24
  
  write(toJSON(x, auto_unbox = T), Sys.getenv("ADMITAD_TOKEN_PATH"))
  x$access_token
}